package pl.iwona.order;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import pl.iwona.meal.Meal;

public class Order {
    private List<Meal> meals = new ArrayList<>();


    public void addMeal(Meal meal) {
       this.meals.add(meal);
    }

    public void removeMeal(Meal meal) {
        this.meals.remove(meal);
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void cancelOrder() {
        this.meals.clear();
    }

    public int totalPrice(Meal meal) {
        int sum = 0;
        for (int i = 0; i < meals.size(); i++) {
            int price = meal.getPrice();
            sum = sum + price;
        }
        return sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(meals, order.meals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meals);
    }

    @Override
    public String toString() {
        return "Order{" +
                "meals=" + meals +
                '}';
    }
}
