package pl.iwona.order;

public enum OrderStatus {
    ORDERED, READY, DELIVERED
}
