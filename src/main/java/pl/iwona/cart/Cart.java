package pl.iwona.cart;

import java.util.ArrayList;
import java.util.List;
import pl.iwona.meal.Meal;
import pl.iwona.order.Order;

public class Cart {
    private List<Order> orders = new ArrayList<>();

    public List<Order> getOrders() {
        return orders;
    }

    public void addToCart(Order order) {
        orders.add(order);
    }

    void clearCart() {
        this.orders.clear();
    }

    public void simulateLargeOrder() {
        for (int i = 0; i < 1_000; i++) {
            Meal meal = new Meal(i % 10, "pizza no. " + i);
            Order order = new Order();
            order.addMeal(meal);
            addToCart(order);
        }
        System.out.println("Cart size: " + orders.size());
        clearCart();
    }
}
