package pl.iwona.meal;

import java.util.Objects;

public class Meal {
    private int price;
    private int quantity;
    private String name;

    public Meal() {
    }

    public Meal(int price) {
        this.price = price;
    }

    public Meal(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public Meal(int price, int quantity, String name) {
        this.price = price;
        this.quantity = quantity;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public int getDiscountedPrice(int discount) {
        if (discount > price) {
            throw new IllegalArgumentException("Discount cannot be higher than the price!");
        }
        return this.price - discount;
    }

    public int getSum(){
        return price * quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meal meal = (Meal) o;
        return price == meal.price &&
                quantity == meal.quantity &&
                Objects.equals(name, meal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, quantity, name);
    }

    @Override
    public String toString() {
        return "Meal{" +
                "price=" + price +
                ", quantity=" + quantity +
                ", name='" + name + '\'' +
                '}';
    }
}
