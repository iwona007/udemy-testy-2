package pl.iwona.account;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;

class AccountTest {

    @Test
    void new_account_should_not_be_active_after_creation() {
        Account account = new Account();

        assertFalse(account.isActive()); //junit.jupiter
        assertThat(account.isActive(), equalTo(false)); //hamcrest
        assertThat(account.isActive(), is(false));//hamcrest
        assertThat(account.isActive()).isFalse();
    }

    @Test
    void account_should_be_active_after_activation() {
        Account account = new Account();

        account.activate();
        assertThat(account.isActive(), notNullValue()); //hamcrest
        assertThat(account.isActive(), is(notNullValue())); ////hamcrest
        assertTrue(account.isActive());
        assertThat(account.isActive()).isTrue();
    }

    @Test
    void new_created_account_should_not_have_default_deliver_address_test() {
        Account account = new Account();

        Address address = account.getDefaultDeliveryAddress();

        assertEquals(null, address);
        assertNull(address);
        assertThat(address, nullValue());
        assertThat(address).isNull();
    }

    @Test
    void default_delivery_should_not_be_null_test() {
        Address address = new Address("Deszczowa", "5");
        Account account = new Account();

        account.setDefaultDeliveryAddress(address);

        assertNotNull(address);//junit.jupiter expected, actual
        assertThat(address).isNotNull(); //hamcrest.MatcherAssert.assertThat
        assertThat(address, is(notNullValue())); //hamcrest.MatcherAssert.assertThat actual, expected
        assertThat(address, notNullValue());//
    }

//    @Test
    @RepeatedTest(2)
    void new_account_with_not_null_address_should_be_active() {
        Address address = new Address("Deszczowa", "5");
        Account account = new Account(address);

        assumingThat(address != null, () -> {
            assertTrue(account.isActive());
        });
    }
}
