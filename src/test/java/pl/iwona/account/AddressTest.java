package pl.iwona.account;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @ParameterizedTest
    @CsvSource({"Deszczowa, 8", "Tęczowa, 5", "'Romka, Tomka', 5"})
    void given_addresses_should_not_be_empty_and_have_proper_names_test(String name, String nr) {
        assertThat(name, notNullValue());
        assertThat(name, containsString("a"));
        assertThat(nr, notNullValue());
        assertThat(nr.length(), lessThanOrEqualTo(8));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/addresses.csv")
    void addresses_from_file_should_not_be_empty_and_have_proper_names_test(String name, String nr) {
        assertThat(name, notNullValue());
        assertThat(name, containsString("a"));
        assertThat(nr, notNullValue());
        assertThat(nr.length(), lessThanOrEqualTo(8));
    }
}
