package pl.iwona.order;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.jupiter.api.*;
import pl.iwona.meal.Meal;

import static org.junit.jupiter.api.Assertions.*;

class OrderBackupTest {
    private static OrderBackup orderBackup;

    @BeforeAll
    static void setUp() throws FileNotFoundException {
        orderBackup = new OrderBackup();
        orderBackup.createFile();
    }

    @BeforeEach
    void append_at_the_start_of_the_line() throws IOException {
        orderBackup.getWriter().append("New order: ");
    }

    @AfterEach
    void append_at_the_end_of_the_line() throws IOException {
        orderBackup.getWriter().append(" backed up");
    }

    @Test
        //test ktory wykonuje zamowienie
    void backupOrderWithOneMeal() throws IOException {
        Meal meal = new Meal(7, "Fries");
        Order order = new Order();

        order.addMeal(meal);
        orderBackup.backupOrder(order);
        System.out.println("Order: " + order.toString() + " bucked up");
    }

    @AfterAll
    static void tearDown() throws IOException {
        orderBackup.closeFile();
    }
}
