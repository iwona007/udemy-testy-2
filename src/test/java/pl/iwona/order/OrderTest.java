package pl.iwona.order;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.iwona.meal.Meal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    private Order order;

    @BeforeEach
    void initOrder() {
        order = new Order();
    }

    @AfterEach
    void cleanUp() {
        System.out.println("clean List");
        order.cancelOrder();
    }

    @Test
    void test_assert_array_equals() {
        int[] array1 = {1, 2, 3};
        int[] array2 = {1, 2, 3};
        assertArrayEquals(array1, array2);
    }

    @Test
    void meal_list_should_be_empty_after_creation_of_order() {
//        Order order = new Order(); BeforeEach przejelo inicjalizwanie obiektu order

        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));
        assertThat(order.getMeals(), hasSize(0));
    }

    @Test
    void adding_meal_to_order_should_increase_order_size() {
        Meal meal1 = new Meal(10, "pizza");
        Meal meal2 = new Meal(15, "hamburger");

        order.addMeal(meal1);
        order.addMeal(meal2);

        assertThat(order.getMeals(), hasSize(2));
//        assertThat(order.getMeals(), contains(meal1));
        assertThat(order.getMeals(), hasItem(meal2));
    }

    @Test
    void remove_meal_to_order_should_decrease_order_size() {

        Meal meal1 = new Meal(15, "pizza");
        Meal meal2 = new Meal(10, "hamburger");

        order.addMeal(meal1);
        order.addMeal(meal2);
        order.removeMeal(meal1);
        order.removeMeal(meal2);

        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(), not(contains(meal1)));
        assertThat(order.getMeals(), not(contains(meal2)));
    }

    @Test
    void meal_should_be_in_a_right_order_after_adding_them_to_order() {

        Meal meal1 = new Meal(15, "pizza");
        Meal meal2 = new Meal(25, "hamburger");

        order.addMeal(meal1);
        order.addMeal(meal2);

        assertThat(order.getMeals(), contains(meal1, meal2));
        assertThat(order.getMeals(), containsInAnyOrder(meal2, meal1)); //jaka kolwiekkolejnosc
        assertThat(order.getMeals().get(0), is(meal1));
        assertThat(order.getMeals().get(1), is(meal2));
    }

    @Test
    void test_if_two_meal_lists_not_are_the_same() {
        Meal meal1 = new Meal(15, "Burger");
        Meal meal2 = new Meal(5, "Sandwich");
        Meal meal3 = new Meal(11, "kebab");

        List<Meal> list1 = Arrays.asList(meal1, meal2);
        List<Meal> list2 = Arrays.asList(meal3);

        assertThat(list1, not(equalTo(list2)));
        assertThat(list1, not(is(list2)));
    }

    @Test
    void test_if_two_meal_lists_are_the_same() {
        Meal meal1 = new Meal(15, "Burger");
        Meal meal2 = new Meal(5, "Sandwich");
        Meal meal3 = new Meal(11, "kebab");

        List<Meal> list1 = Arrays.asList(meal1, meal2);
        List<Meal> list2 = Arrays.asList(meal1, meal2);

        assertThat(list1, is(list2));
        assertThat(list1, equalTo(list2));
    }

    @Test
    void canceling_order_should_remove_all_items_from_meals_list() {

        Meal meal1 = new Meal(Integer.MAX_VALUE, "pizza");
        Meal meal2 = new Meal(Integer.MAX_VALUE, "Sandwich");

        order.addMeal(meal1);
        order.addMeal(meal2);
        order.cancelOrder();

        assertThat(order.getMeals().size(), is(0));
    }
}
