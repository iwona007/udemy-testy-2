package pl.iwona.meal;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MealTest {

    @Test
    void should_return_discounted_price_test() {
        Meal meal = new Meal(35);
        int discountedPrice = meal.getDiscountedPrice(7);

        assertEquals(28, discountedPrice); // junit jupter
        assertThat(discountedPrice).isEqualTo(28); //assertJ
        assertThat(discountedPrice, equalTo(28));//hamcrasst
    }

    @Test
    void references_for_the_same_object_should_be_equals() {
        Meal meal = new Meal(35);
        Meal meal2 = meal;

        assertSame(meal, meal2); //unit.jupiter
        assertThat(meal, sameInstance(meal2)); //hamcrest.Matchers.
        assertThat(meal).isSameAs(meal2); //unit.jupiter
    }

    @Test
    void reference_for_different_object_should_not_be_equals() {
        Meal meal = new Meal(35);
        Meal meal2 = new Meal(40);

        assertNotSame(meal, meal2); //unit.jupiter
        assertThat(meal, not(sameInstance(meal2)));//hamcrest.Matchers.
        assertThat(meal).isNotSameAs(meal2); //unit.jupiter
    }

    @Test
    void two_meals_should_be_equals_when_price_and_name_are_the_same() {
        Meal meal = new Meal(30, "pizza");
        Meal meal2 = new Meal(30, "pizza");

        assertEquals(meal, meal2);
    }

    @Test
    void exception_should_be_thrown_if_discount_is_higher_than_the_price() {
        Meal meal = new Meal(30);

        assertThrows(IllegalArgumentException.class, () -> meal.getDiscountedPrice(50));
    }

    @Test
    void test_meal_sum_price() {
        Meal meal = mock(Meal.class);

        given(meal.getPrice()).willReturn(15);
        given(meal.getQuantity()).willReturn(3);

        given(meal.getSum()).willCallRealMethod();

        int sumPrice = meal.getSum();

        assertThat(sumPrice, equalTo(45));
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10, 15, 18})
    void mealPricesShouldBeLowerThan20(int price) {
        assertThat(price, lessThan(20));
    }

    @ParameterizedTest
    @MethodSource("create_meal_with_name_and_price_test")
    void meal_should_have_correct_name_and_price_test(String name, int price) {
        assertThat(name, containsString("Pizza"));
        assertThat(price, greaterThanOrEqualTo(15));
    }

    private static Stream<Arguments> create_meal_with_name_and_price_test() {
        return Stream.of(
                Arguments.of("Pizza", 15),
                Arguments.of("Pizza", 20)
        );
    }

    @ParameterizedTest
    @MethodSource("create_meals_name")
    void meal_name_should_end_with_meal_test(String name) {
        assertThat(name, notNullValue());
        assertThat(name, endsWith("meal"));
    }

    private static Stream<String> create_meals_name() {
        List<String> names = Arrays.asList("Hamburgermeal", "Tortillameal", "Nachosmeal");
        return names.stream();
    }
}
